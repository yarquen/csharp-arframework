﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace RARPG_Detector
{
    /// <summary>
    /// Interaction logic for RARPG_Projection.xaml
    /// </summary>
    public partial class RARPG_Projection : Window
    {
        public static RARPG_Projection me;
        public static Random r;
        Timer timer;

        public RARPG_Projection()
        {
            InitializeComponent();

            r = new Random(DateTime.Now.Millisecond);

            timer = new Timer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = 2000;
            timer.Start();

            System.Drawing.Rectangle wa = Screen.AllScreens[0].WorkingArea;
            this.Left = wa.Left;
            this.Top = wa.Top;
            this.Width = 800;
            this.Height = 600;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (canvasAll.Children.Count >= 80)
                canvasAll.Children.Clear();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Maximized;

            me = this;
        }

        public static void SetRectOn(Point p)
        {
            Ellipse ball = new Ellipse();
            ball.Width = 10;
            ball.Height = 10;
            
            ball.Fill = Brushes.Green;
           
            Canvas.SetLeft(ball, p.X - 5);
            Canvas.SetTop(ball, p.Y - 5);

            me.canvasAll.Children.Add(ball);
        }
    }
}
