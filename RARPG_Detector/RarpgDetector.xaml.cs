﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections;
using DotNetMatrix;
using RARPG_Detector.General;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;
using Emgu.CV.Structure;
using Microsoft.Research.Kinect.Nui;
using System.Diagnostics;

namespace RARPG_Detector
{
    /// <summary>
    /// Interaction logic for RarpgDetector.xaml
    /// </summary>
    public partial class RarpgDetector : Window
    {
        #region Fields
        // Kinect
        Microsoft.Research.Kinect.Nui.Runtime nui;
        
        const int RED_IDX = 2;                              // Index to access Red color on a RGB byte
        const int GREEN_IDX = 1;                            // Index to access Green color on a RGB byte
        const int BLUE_IDX = 0;                             // Index to access Blue channel on a RGB byte

        const int POINTS_TO_GENERATE = 70000;               // Number of points to generate in the polygon to determine the plane equation

        int angle;                                          // Kinect's angle

        byte[] depthFrame32 = new byte[320 * 240 * 4];      // Depth's byte array
        short[] realDepthArray = new short[320 * 240];      // Real depth array
        byte[] bitsToPlotter = new byte[320 * 240 * 4];     // Byte array for plotter

        bool showDepth = true;                              // Shows Depth Image or Plane Image

        double a, b, c, d, t, g;                            // Variables used in 2D transformation

        ArrayList realWorldPointsToPlane;                   // Points used to find the plane equation
        private Plano tablePlane;                           // Surface found

        Polygon polygon;                                    // Polygon to determine the plan

        ArrayList bounds;                                   // Four vertices, to build the plane

        RARPG_Projection projectionScreen;                  // The projection Screen

        double minPrecision = -0.95, maxPrecision = -1.05;  // Precisions to see if the point is in the plane

        double minX, minY, maxX, maxY;                      // Min, max from bounds (as images)

        double bMinX, bMinY, bMinZ, bMaxX, bMaxY, bMaxZ;    // Bounds in real world

        DateTime lastTime = DateTime.MaxValue;

        bool foundDepthsForSurface = false;

        List<short[]> realDepthArrays = new List<short[]>();

        List<Dictionary<short, int>> histograms = new List<Dictionary<short, int>>();

        short[] d_surfacePPixel = new short[320 * 240];

        Emgu.CV.Image<Gray, Byte> img;
        #endregion

        #region Virtual Click
        [DllImport("user32.dll")]
        static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        [Flags]
        public enum MouseEventFlags
        {
            LEFTDOWN = 0x00000002,
            LEFTUP = 0x00000004,
            MIDDLEDOWN = 0x00000020,
            MIDDLEUP = 0x00000040,
            MOVE = 0x00000001,
            ABSOLUTE = 0x00008000,
            RIGHTDOWN = 0x00000008,
            RIGHTUP = 0x00000010
        }

        public static void LeftClick(int x, int y)
        {
            System.Windows.Forms.Cursor.Position = new System.Drawing.Point(x, y);
            mouse_event((int)(MouseEventFlags.LEFTDOWN), 0, 0, 0, 0);
            mouse_event((int)(MouseEventFlags.LEFTUP), 0, 0, 0, 0);
        }
        #endregion

        public RarpgDetector()
        {
            InitializeComponent();

            this.realWorldPointsToPlane = new ArrayList();

            bounds = new ArrayList();

            polygon = new Polygon();

            img = new Emgu.CV.Image<Gray, Byte>(320, 240);

            //polygon.MouseUp += new MouseButtonEventHandler(parallelogram_MouseUp);

            canvasVideo.Visibility = System.Windows.Visibility.Visible;

            canvasDepth.Visibility = System.Windows.Visibility.Visible;

            //videoDepth.Child = new System.Windows.Forms.PictureBox();

            //canvasPlotter.Visibility = System.Windows.Visibility.Visible;
        }

        void parallelogram_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Point aux = e.GetPosition(videoRGB);
            Point pScreen = getScreenPoint(aux);

            Debug.WriteLine(" --> Desenhar em: " + pScreen.ToString());
            Debug.WriteLine("   --> Mundo Real: " + getRealWorldPoint(aux).ToString());

            RARPG_Projection.SetRectOn(pScreen);
        }

        #region init
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < Runtime.Kinects.Count; i++)
                if (Runtime.Kinects[i].Status == KinectStatus.Connected)
                    nui = Runtime.Kinects[i];
        }
        #endregion

        #region Mostrar Videos
        void nui_ColorFrameReady(object sender, ImageFrameReadyEventArgs e)
        {
            PlanarImage Image = e.ImageFrame.Image;

            System.Drawing.Bitmap bmp = PImageToBitmap(Image);

            Emgu.CV.Image<Rgb, byte> img = new Emgu.CV.Image<Rgb, byte>(bmp);

            img = img.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);

            videoRGB.Source = ToBitmapSource(img);
            //videoRGB.Source = BitmapSource.Create(Image.Width, Image.Height, 96, 96, PixelFormats.Bgr32, null, Image.Bits, Image.Width * Image.BytesPerPixel);
        }

        public System.Drawing.Bitmap PImageToBitmap(PlanarImage PImage)
        {
            System.Drawing.Bitmap bmap = new System.Drawing.Bitmap(
            PImage.Width,
            PImage.Height,
            System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            System.Drawing.Imaging.BitmapData bmapdata = bmap.LockBits(new System.Drawing.Rectangle(0, 0, PImage.Width, PImage.Height),
                System.Drawing.Imaging.ImageLockMode.WriteOnly,
                bmap.PixelFormat);
            IntPtr ptr = bmapdata.Scan0;
            Marshal.Copy(PImage.Bits,
            0,
            ptr,
            PImage.Width *
            PImage.BytesPerPixel *
            PImage.Height);
            bmap.UnlockBits(bmapdata);

            return bmap;
        }

        void nui_DepthFrameReady(object sender, ImageFrameReadyEventArgs e)
        {
            PlanarImage Image = e.ImageFrame.Image;

            // With Histograms
            byte[] convertedDepthFrame = convertDepthFrame_approach1(Image.Bits);
            
            // With Plane Detection
            //byte[] convertedDepthFrame = convertDepthFrame(Image.Bits);

            BitmapSource bs = BitmapSource.Create(Image.Width, Image.Height, 96, 96, PixelFormats.Bgr32, null, convertedDepthFrame, Image.Width * 4);

            Emgu.CV.Image<Rgb, Byte> img = new Emgu.CV.Image<Rgb, Byte>(_bitmapFromSource(bs));

            img = img.SmoothMedian(3);
            //img = img.Erode(1);
            img = img.Dilate(1);

            Emgu.CV.Image<Gray, Byte> img_novo = img.Convert<Gray, Byte>();

            Emgu.CV.Contour<System.Drawing.Point> contornos = img_novo.FindContours();

            if (contornos != null && tablePlane != null)
            {
                //Debug.WriteLine(">> Contornos encontrados.");
                //Debug.WriteLine(contornos.BoundingRectangle.ToString());
                //RARPG_Projection.SetRectOn(getScreenPoint(new Point(contornos.BoundingRectangle.X, contornos.BoundingRectangle.Y)));
                //Point p = getScreenPoint(new Point(contornos.BoundingRectangle.X, contornos.BoundingRectangle.Y));
                //Debug.WriteLine("Clique em " + p.ToString());
                //LeftClick((int)p.X, (int)p.Y);
            }

            videoDepth.Source = ToBitmapSource(img);
        }

        private System.Drawing.Bitmap _bitmapFromSource(BitmapSource bitmapsource)
        {
            System.Drawing.Bitmap bitmap;
            using (MemoryStream outStream = new MemoryStream())
            {
                // from System.Media.BitmapImage to System.Drawing.Bitmap 
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new System.Drawing.Bitmap(outStream);
            }
            return bitmap;
        }

        /// <summary>
        /// Delete a GDI object
        /// </summary>
        /// <param name="o">The poniter to the GDI object to be deleted</param>
        /// <returns></returns>
        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr o);

        /// <summary>
        /// Convert an IImage to a WPF BitmapSource. The result can be used in the Set Property of Image.Source
        /// </summary>
        /// <param name="image">The Emgu CV Image</param>
        /// <returns>The equivalent BitmapSource</returns>
        public static BitmapSource ToBitmapSource(Emgu.CV.IImage image)
        {
            using (System.Drawing.Bitmap source = image.Bitmap)
            {
                IntPtr ptr = source.GetHbitmap(); //obtain the Hbitmap

                BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    ptr,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

                DeleteObject(ptr); //release the HBitmap
                return bs;
            }
        }

        byte[] convertToPlotter()
        {
            //DateTime cur = DateTime.Now;

            //if (tablePlane != null)
            //{
            #region old
            //int depthX = 0, depthY = 0, bitIndex = 0;
            //short depthValue;

            //for (int i = 0; i < 320; i++)
            //{
            //    for (int j = 0; j < 240; j++)
            //    {
            //        depthX = (int)(320 * i / canvasDepth.ActualWidth);
            //        depthY = (int)(240 * j / canvasDepth.ActualHeight);

            //        depthValue = realDepthArray[(depthY * 320 + depthX)];

            //        // Getting the real point
            //        Microsoft.Research.Kinect.Nui.Vector v = nui.SkeletonEngine.DepthImageToSkeleton((float)depthX, (float)depthY, depthValue);
            //        // Getting the value of the point in the plane equation
            //        double isIn = planoSuperficie.pointIn(v.X, v.Y, v.Z);

            //        if (isIn == 0)
            //        {
            //            bitsToPlotter[bitIndex + RED_IDX] = 255;
            //            bitsToPlotter[bitIndex + GREEN_IDX] = 0;
            //            bitsToPlotter[bitIndex + BLUE_IDX] = 0;
            //        }
            //        else if (isIn > 0)
            //        {
            //            bitsToPlotter[bitIndex + RED_IDX] = 0;
            //            bitsToPlotter[bitIndex + GREEN_IDX] = 255;
            //            bitsToPlotter[bitIndex + BLUE_IDX] = 0;
            //        }
            //        else if (isIn < 0)
            //        {
            //            bitsToPlotter[bitIndex + RED_IDX] = 0;
            //            bitsToPlotter[bitIndex + GREEN_IDX] = 0;
            //            bitsToPlotter[bitIndex + BLUE_IDX] = 255;
            //        }
            //        else
            //        {
            //            bitsToPlotter[bitIndex + RED_IDX] = 0;
            //            bitsToPlotter[bitIndex + GREEN_IDX] = 0;
            //            bitsToPlotter[bitIndex + BLUE_IDX] = 0;
            //        }

            //        bitIndex += 4;
            //    }
            //}
            #endregion

            int bitIndex = 0;
            for (int i = 0; i < realDepthArray.Length; i++)
            {
                short depthValue = realDepthArray[i];
                int depthX = i / 320;
                int depthY = i % 320;

                //if (!PointInPolygon(new Point((double)depthX, (double)depthY), polygon.Points.ToArray<Point>()))
                //    continue;

                Microsoft.Research.Kinect.Nui.Vector v = nui.SkeletonEngine.DepthImageToSkeleton((float)depthX, (float)depthY, depthValue);
                //Microsoft.Research.Kinect.Nui.Vector v = DepthToWorld(depthX, depthY, depthValue);

                //if (!(v.X >= bMinX && v.X <= bMaxX ||
                //      v.Y >= bMinY && v.Y <= bMaxY ||
                //      v.Z >= bMinZ && v.Z <= bMaxZ))
                //{
                //    continue;
                //}

                double isInPlane = tablePlane.pointIn(v.X, v.Y, v.Z);

                if (isInPlane >= sliderMin.Value && isInPlane <= sliderMax.Value)
                {
                    bitsToPlotter[bitIndex + RED_IDX] = 0;
                    bitsToPlotter[bitIndex + GREEN_IDX] = 0;
                    bitsToPlotter[bitIndex + BLUE_IDX] = 255;
                }
                else if (isInPlane >= sliderTouchMin.Value && isInPlane <= sliderTouchMax.Value)
                {
                    // RARPG_Projection.ShowSomething();
                    bitsToPlotter[bitIndex + RED_IDX] = 255;
                    bitsToPlotter[bitIndex + GREEN_IDX] = 0;
                    bitsToPlotter[bitIndex + BLUE_IDX] = 0;
                }
                else
                {
                    bitsToPlotter[bitIndex + 3] = 0;
                }

                bitIndex += 4;
            }
            //}

            //lastTime = cur;

            return bitsToPlotter;
        }

        DateTime lastCheck = DateTime.Now;
        double isInPlane;

        byte[] convertDepthFrame_approach1(byte[] depthFrame16)
        {
            int iDepth = 0;
            for (int i16 = 0, i32 = 0;
                i16 < depthFrame16.Length && i32 < depthFrame32.Length;
                i16 += 2, i32 += 4)
            {
                int realDepth = (int)depthFrame16[i16] | depthFrame16[i16 + 1] << 8;

                realDepthArray[i16 / 2] = (short)realDepth;

                byte intensity = (byte)(255 - (255 * realDepth / 0x0fff));

                if (!foundDepthsForSurface)
                {
                    depthFrame32[i32 + RED_IDX] = 0;
                    depthFrame32[i32 + GREEN_IDX] = 0;
                    depthFrame32[i32 + BLUE_IDX] = 0;

                    continue;
                }

                short surface = d_surfacePPixel[iDepth++];

                // Testes para ver se os histogramas funcionam

                //if (realDepth == 0)
                //{
                //    depthFrame32[i32 + RED_IDX] = 255;
                //    depthFrame32[i32 + GREEN_IDX] = 0;
                //    depthFrame32[i32 + BLUE_IDX] = 0;
                //}
                //else if (realDepth == surface)
                //{
                //    depthFrame32[i32 + RED_IDX] = 255;
                //    depthFrame32[i32 + GREEN_IDX] = 255;
                //    depthFrame32[i32 + BLUE_IDX] = 255;
                //}
                //else if (realDepth < surface)
                //{
                //    depthFrame32[i32 + RED_IDX] = 0;
                //    depthFrame32[i32 + GREEN_IDX] = 255;
                //    depthFrame32[i32 + BLUE_IDX] = 0;
                //}
                //else if (realDepth > surface)
                //{
                //    depthFrame32[i32 + RED_IDX] = 0;
                //    depthFrame32[i32 + GREEN_IDX] = 0;
                //    depthFrame32[i32 + BLUE_IDX] = 255;
                //}
                //else
                //{
                //    depthFrame32[i32 + RED_IDX] = 0;
                //    depthFrame32[i32 + GREEN_IDX] = 0;
                //    depthFrame32[i32 + BLUE_IDX] = 0;
                //}

                if (realDepth < surface - 10 && realDepth > surface - 20)
                {
                    depthFrame32[i32 + RED_IDX] = 255;
                    depthFrame32[i32 + GREEN_IDX] = 255;
                    depthFrame32[i32 + BLUE_IDX] = 255;
                }
                else
                {
                    depthFrame32[i32 + RED_IDX] = 0;
                    depthFrame32[i32 + GREEN_IDX] = 0;
                    depthFrame32[i32 + BLUE_IDX] = 0;
                }

                //if (realDepth >= d_surfacePPixel[i16 / 2] - 5 && realDepth <= d_surfacePPixel[i16 / 2] + 5)
                //{
                //    depthFrame32[i32 + RED_IDX] = 0;
                //    depthFrame32[i32 + GREEN_IDX] = 0;
                //    depthFrame32[i32 + BLUE_IDX] = 0;
                //}
            }

            return depthFrame32;
        }

        byte[] convertDepthFrame(byte[] depthFrame16)
        {
            int iDepth = 0;

            for (int i16 = 0, i32 = 0;
                i16 < depthFrame16.Length && i32 < depthFrame32.Length;
                i16 += 2, i32 += 4)
            {
                int realDepth = (int)depthFrame16[i16] | depthFrame16[i16 + 1] << 8;
                //int realDepth = (depthFrame16[i16 + 1] << 5) | (depthFrame16[i16] >> 3);
                realDepthArray[i16 / 2] = (short)realDepth;

                byte intensity = (byte)(255 - (255 * realDepth / 0x0fff));

                int depthX = iDepth / 320;
                int depthY = iDepth % 320;

                Microsoft.Research.Kinect.Nui.Vector v = new Microsoft.Research.Kinect.Nui.Vector();
                isInPlane = 10000;

                if (tablePlane != null)
                {
                    v = nui.SkeletonEngine.DepthImageToSkeleton((float)depthX, (float)depthY, (short)realDepth);
                    isInPlane = tablePlane.pointIn(v.X, v.Y, v.Z);
                }

                //double planeMin = sliderMin.Value, planeMax = sliderMax.Value;
                double touchMin = -0.98, touchMax = -0.95;

                if (tablePlane == null)
                {
                    depthFrame32[i32 + RED_IDX] = intensity;
                    depthFrame32[i32 + GREEN_IDX] = intensity;
                    depthFrame32[i32 + BLUE_IDX] = intensity;

                    continue;
                }

                if (isInPlane >= -0.98 && isInPlane <= -0.97)
                {
                    depthFrame32[i32 + RED_IDX] = 0;
                    depthFrame32[i32 + GREEN_IDX] = 255;
                    depthFrame32[i32 + BLUE_IDX] = 0;

                    int s = DateTime.Now.Subtract(lastCheck).Seconds;
                    if (s >= 4 && s <= 6)
                    {
                        //RARPG_Projection.SetRectOn(getScreenPoint(new Point(iDepth / 320, iDepth % 320)));
                        lastCheck = DateTime.Now;
                    }
                }
                else
                {
                    depthFrame32[i32 + RED_IDX] = 0;
                    depthFrame32[i32 + GREEN_IDX] = 0;
                    depthFrame32[i32 + BLUE_IDX] = 0;
                }

                iDepth++;

                //if (isInPlane < -1.01)
                //{
                //    depthFrame32[i32 + RED_IDX] = 0;
                //    depthFrame32[i32 + GREEN_IDX] = 0;
                //    depthFrame32[i32 + BLUE_IDX] = 0;
                //}
                //else if (isInPlane >= -1.01 && isInPlane <= -0.97)
                //{
                //    depthFrame32[i32 + RED_IDX] = 0;
                //    depthFrame32[i32 + GREEN_IDX] = 0;
                //    depthFrame32[i32 + BLUE_IDX] = 0;
                //}
                //else if (isInPlane >= -0.97 && isInPlane < -0.90)
                //{
                //    depthFrame32[i32 + RED_IDX] = 0;
                //    depthFrame32[i32 + GREEN_IDX] = 255;
                //    depthFrame32[i32 + BLUE_IDX] = 0;
                //}
                //else //if (isInPlane > -0.97)
                //{
                //    depthFrame32[i32 + RED_IDX] = 0;
                //    depthFrame32[i32 + GREEN_IDX] = 0;
                //    depthFrame32[i32 + BLUE_IDX] = 0;
                //}
            }
            return depthFrame32;

        }
        #endregion

        private void Window_Closed(object sender, EventArgs e)
        {
            if (nui != null)
                if (nui.NuiCamera != null)
                    nui.Uninitialize();

            if (projectionScreen != null)
                projectionScreen.Close();

            Environment.Exit(0);
        }

        private void canvasDepth_MouseUp(object sender, MouseButtonEventArgs e)
        {
            // getRealWorldPoint(e, videoDepth, canvasDepth);
        }

        private void canvasVideo_MouseUp(object sender, MouseButtonEventArgs e)
        {
            // If the four points has been chosen, lets stop it
            if (bounds.Count == 4)
            {
                System.Windows.Forms.MessageBox.Show("Já foram selecionados 4 pontos.");
                return;
            }

            Point p = e.GetPosition(videoRGB);
            bounds.Add(p);
            // Add the point found to our pointsToPlane array
            this.realWorldPointsToPlane.Add(getRealWorldPoint(p));

            #region Adding visual point
            Rectangle r = new Rectangle();
            r.Width = 3; r.Height = 3;
            r.Fill = Brushes.Azure;
            Canvas.SetLeft(r, p.X - 1.5);
            Canvas.SetTop(r, p.Y - 1.5);

            Rectangle r2 = new Rectangle();
            r2.Width = 3; r2.Height = 3;
            r2.Fill = Brushes.Red;
            Canvas.SetLeft(r2, p.X - 1.5);
            Canvas.SetTop(r2, p.Y - 1.5);

            canvasVideo.Children.Add(r);
            canvasDepth.Children.Add(r2);
            #endregion

        }

        /// <summary>
        /// Returns the real world cartesian point, based on the device's position.
        /// </summary>
        /// <param name="p">The point clicked on the image</param>
        /// <returns>The real world Vector</returns>
        private Microsoft.Research.Kinect.Nui.Vector getRealWorldPoint(Point p)
        {
            int depthX = (int)(320 * p.X / canvasVideo.ActualWidth);
            int depthY = (int)(240 * p.Y / canvasVideo.ActualHeight);
            short depthValue = realDepthArray[depthY * 320 + depthX];
            Microsoft.Research.Kinect.Nui.Vector v = nui.SkeletonEngine.DepthImageToSkeleton(depthX, depthY, depthValue);

            return v;
        }

        static bool PointInPolygon(Point p, Point[] poly)
        {
            Point p1, p2;

            bool inside = false;

            if (poly.Length < 3)
            {
                return inside;
            }

            Point oldPoint = new Point(
            poly[poly.Length - 1].X, poly[poly.Length - 1].Y);

            for (int i = 0; i < poly.Length; i++)
            {
                Point newPoint = new Point(poly[i].X, poly[i].Y);

                if (newPoint.X > oldPoint.X)
                {
                    p1 = oldPoint;
                    p2 = newPoint;
                }
                else
                {
                    p1 = newPoint;
                    p2 = oldPoint;
                }

                if ((newPoint.X < p.X) == (p.X <= oldPoint.X)
                && ((long)p.Y - (long)p1.Y) * (long)(p2.X - p1.X)
                 < ((long)p2.Y - (long)p1.Y) * (long)(p.X - p1.X))
                {
                    inside = !inside;
                }

                oldPoint = newPoint;
            }

            return inside;
        }

        double RawDepthToMeters(short depthValue)
        {
            if (depthValue < 2047)
            {
                return (float)(1.0 / (double)(depthValue * -0.0030711016 + 3.3309495161));
            }

            return 0.0;
        }

        Microsoft.Research.Kinect.Nui.Vector DepthToWorld(int x, int y, short depthValue)
        {
            const double fx_d = 1.0 / 5.9421434211923247e+02;
            const double fy_d = 1.0 / 5.9104053696870778e+02;
            const double cx_d = 3.3930780975300314e+02;
            const double cy_d = 2.4273913761751615e+02;

            Microsoft.Research.Kinect.Nui.Vector result = new Microsoft.Research.Kinect.Nui.Vector();
            double depth = RawDepthToMeters(depthValue);

            result.X = (float)((x - cx_d) * depth * fx_d);
            result.Y = (float)((y - cy_d) * depth * fy_d);
            result.Z = (float)(depth);

            return result;
        }

        /// <summary>
        /// Finds a plane equation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEncontrarPlano_Click(object sender, RoutedEventArgs e)
        {
            // Bound not chosen
            if (bounds.Count < 4)
            {
                System.Windows.Forms.MessageBox.Show("Escolha os 4 pontos limitantes");
                return;
            }

            // Has chosen the bounds, but the plane hasn't been found yet
            if (bounds.Count == 4 && tablePlane == null)
            {
                #region Defining minX and minY
                List<double> minX = new List<double>();
                List<double> minY = new List<double>();

                foreach (object point in bounds)
                {
                    minX.Add(((Point)point).X);
                    minY.Add(((Point)point).Y);
                }

                minX.Sort(); minY.Sort();

                this.minX = minX[0]; maxX = minX[minX.Count - 1];
                this.minY = minY[0]; maxY = minY[minY.Count - 1];
                #endregion

                #region Defining minX, minY, minZ and maxX, maxY, maxZ of the real world
                List<double> _X = new List<double>();
                List<double> _Y = new List<double>();
                List<double> _Z = new List<double>();

                foreach (object point in bounds)
                {
                    Microsoft.Research.Kinect.Nui.Vector v = getRealWorldPoint((Point)point);
                    _X.Add(v.X);
                    _Y.Add(v.Y);
                    _Z.Add(v.Z);
                }

                _X.Sort(); _Y.Sort(); _Z.Sort();


                bMinX = _X[0]; bMinY = _Y[0]; bMinZ = _Z[0];
                bMaxX = _X[_X.Count - 1]; bMaxY = _Y[_Y.Count - 1]; bMaxZ = _Z[_Z.Count - 1];
                #endregion

                #region Drawing the Parallelogram
                polygon.Points = new PointCollection(new List<Point>()
                {
                    (Point) bounds[0], (Point) bounds[1], (Point) bounds[2], (Point) bounds[3]
                });

                polygon.Stroke = Brushes.Chocolate;
                polygon.Fill = Brushes.Chocolate;
                polygon.StrokeThickness = 2;
                polygon.Opacity = 0.6;

                canvasVideo.Children.Add(polygon);
                #endregion

                // Adding the bounds points to the pointsToPlane array
                realWorldPointsToPlane.Add(getRealWorldPoint((Point)bounds[0]));
                realWorldPointsToPlane.Add(getRealWorldPoint((Point)bounds[1]));
                realWorldPointsToPlane.Add(getRealWorldPoint((Point)bounds[2]));
                realWorldPointsToPlane.Add(getRealWorldPoint((Point)bounds[3]));

                #region Generation the points to find plane equation
                int i = 0;
                Random r = new Random(DateTime.Now.Millisecond);
                Point aux;

                do
                {
                    do
                    {
                        aux = new Point((double)r.Next((int)minX[0], (int)minX[3]), (double)r.Next((int)minY[0], (int)minY[3]));
                    } while (!PointInPolygon(aux, polygon.Points.ToArray<Point>()));

                    realWorldPointsToPlane.Add(getRealWorldPoint(aux));
                } while (i++ < POINTS_TO_GENERATE);
                #endregion
            }

            GeneralMatrix M = new GeneralMatrix(realWorldPointsToPlane.Count, 3);
            GeneralMatrix Mt /*Transposta*/, R = new GeneralMatrix(realWorldPointsToPlane.Count, 1);

            for (int i = 0; i < realWorldPointsToPlane.Count; i++)
            {
                M.SetElement(i, 0, ((Microsoft.Research.Kinect.Nui.Vector)realWorldPointsToPlane[i]).X);
                M.SetElement(i, 1, ((Microsoft.Research.Kinect.Nui.Vector)realWorldPointsToPlane[i]).Y);
                M.SetElement(i, 2, ((Microsoft.Research.Kinect.Nui.Vector)realWorldPointsToPlane[i]).Z);
                R.SetElement(i, 0, -1); // Adding -1 to the right matrix
            }

            Mt = M.Transpose();

            // Multiplicando M pela sua transposta
            M = Mt.Multiply(M);
            // Multiplicando R pela transposta de M
            R = Mt.Multiply(R);

            GeneralMatrix I = M.Solve(R);

            tablePlane = new Plano()
            {
                A = I.GetElement(0, 0),
                B = I.GetElement(1, 0),
                C = I.GetElement(2, 0)
            };

            Debug.WriteLine(" --------- Plano encontrado --------- ");
            Debug.WriteLine("Bounds: ");
            foreach (object point in bounds)
            {
                Microsoft.Research.Kinect.Nui.Vector v = getRealWorldPoint((Point)point);
                Debug.WriteLine("  --> (" + v.X + ", " + v.Y + ", " + v.Z + ", " + v.W + "); No plano: " + tablePlane.pointIn(v.X, v.Y, v.Z));
            }

            Debug.WriteLine("Numero de pontos utilizados: " + realWorldPointsToPlane.Count);
            Debug.WriteLine("Eq do plano: " + tablePlane.ToString());

            Debug.WriteLine("Min/Max:");
            Debug.WriteLine(" ---> MinX: " + bMinX + ", MaxX: " + bMaxX);
            Debug.WriteLine(" ---> MinY: " + bMinY + ", MaxY: " + bMaxY);
            Debug.WriteLine(" ---> MinZ: " + bMinZ + ", MaxZ: " + bMaxZ);

            Debug.WriteLine(" ------------------------------------ ");
        }

        /// <summary>
        /// Tilts the camera up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUp_Click(object sender, RoutedEventArgs e)
        {
            if (nui.NuiCamera.ElevationAngle == Camera.ElevationMaximum)
                return;

            nui.NuiCamera.ElevationAngle = ++angle;
        }
            
        /// <summary>
        /// Tilts the camera down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDown_Click(object sender, RoutedEventArgs e)
        {
            if (nui.NuiCamera.ElevationAngle == Camera.ElevationMinimum)
                return;

            nui.NuiCamera.ElevationAngle = --angle;
        }

        /// <summary>
        /// Shows the min and max precision limits
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowMinMax_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.MessageBox.Show("Min: " + sliderMin.Value.ToString() + ", Max: " + sliderMax.Value.ToString());
        }

        private void btnInit_Click(object sender, RoutedEventArgs e)
        {
            nui.Uninitialize();
            nui.Initialize(RuntimeOptions.UseDepthAndPlayerIndex | RuntimeOptions.UseSkeletalTracking | RuntimeOptions.UseColor | RuntimeOptions.UseDepth);

            nui.VideoStream.Open(ImageStreamType.Video, 2, ImageResolution.Resolution640x480, ImageType.Color);
            nui.DepthStream.Open(ImageStreamType.Depth, 2, ImageResolution.Resolution320x240, ImageType.Depth);

            nui.DepthFrameReady += new EventHandler<ImageFrameReadyEventArgs>(nui_DepthFrameReady);
            nui.VideoFrameReady += new EventHandler<ImageFrameReadyEventArgs>(nui_ColorFrameReady);
        }

        private void sliderMin_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            minPrecision = sliderMin.Value;
        }

        private void sliderMax_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            maxPrecision = sliderMax.Value;
        }

        private void btnShowTouchMinMax_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.MessageBox.Show("Min: " + sliderTouchMin.Value + ", Max: " + sliderTouchMax.Value);
        }

        private void btnOpenProjection_Click(object sender, RoutedEventArgs e)
        {
            if (projectionScreen != null)
                return;

            projectionScreen = new RARPG_Projection();
            projectionScreen.Show();
        }

        bool test = true;
        private void btnTestProjection_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Makes the 2D transformation for clicking map
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCalibrateProjection_Click(object sender, RoutedEventArgs e)
        {
            Point[] positionKinect = new Point[3];
            Point[] positionScreen = new Point[3];

            //positionKinect[0] = (Point)bounds[0];
            //positionKinect[1] = (Point)bounds[1];
            //positionKinect[2] = (Point)bounds[3];

            positionKinect[0] = new Point(0, 0);
            positionKinect[1] = new Point(320, 0);
            positionKinect[2] = new Point(0, 240);

            //positionScreen[0] = new Point(-800, 20);
            //positionScreen[1] = new Point(-10, 21);
            //positionScreen[2] = new Point(-800, 600);

            positionScreen[0] = new Point(0, 0);
            positionScreen[1] = new Point(800, 0);
            positionScreen[2] = new Point(0, 600);

            GeneralMatrix aX = new GeneralMatrix(3, 3);
            GeneralMatrix bX = new GeneralMatrix(3, 1);
            GeneralMatrix aY = new GeneralMatrix(3, 3);
            GeneralMatrix bY = new GeneralMatrix(3, 1);

            for (int i = 0; i < 3; i++)
            {
                aX.SetElement(i, 0, positionKinect[i].X);
                aX.SetElement(i, 1, positionKinect[i].Y);
                aX.SetElement(i, 2, 1);

                aY.SetElement(i, 0, positionKinect[i].X);
                aY.SetElement(i, 1, positionKinect[i].Y);
                aY.SetElement(i, 2, 1);

                bX.SetElement(i, 0, positionScreen[i].X);
                bY.SetElement(i, 0, positionScreen[i].Y);
            }
            GeneralMatrix rX = aX.Solve(bX);
            GeneralMatrix rY = aY.Solve(bY);

            a = rX.GetElement(0, 0);
            c = rX.GetElement(1, 0);
            t = rX.GetElement(2, 0);

            b = rY.GetElement(0, 0);
            d = rY.GetElement(1, 0);
            g = rY.GetElement(2, 0);

            System.Windows.Forms.MessageBox.Show("Mapeamento calibrado!");
        }

        public Point getScreenPoint(Point kinectPoint)
        {
            Point screenPoint = new Point();
            screenPoint.X = (a * kinectPoint.X) + (c * kinectPoint.Y) + t;
            screenPoint.Y = (b * kinectPoint.X) + (d * kinectPoint.Y) + g;

            return screenPoint;
        }

        private void btnSaveDepth_Click(object sender, RoutedEventArgs e)
        {
            StreamWriter sw = new StreamWriter("log/depth.txt");
            sw.WriteLine("Depth Array from test " + DateTime.Now.ToString() + "\n");

            //sw.WriteLine(" --------- Plano encontrado --------- ");
            //sw.WriteLine("Bounds: ");
            //foreach (object point in bounds)
            //{
            //    Point p = (Point)point;

            //    sw.WriteLine(" Como ponto na imagem: ");
            //    sw.WriteLine("   --> (" + p.X + ", " + p.Y + ")");
            //    Microsoft.Research.Kinect.Nui.Vector v = getRealWorldPoint(p);
            //    sw.WriteLine(" Como ponto no mundo real: ");
            //    sw.WriteLine("   --> (" + v.X + ", " + v.Y + ", " + v.Z + ", " + v.W + "); No plano: " + tablePlane.pointIn(v.X, v.Y, v.Z));
            //    sw.WriteLine("");
            //}

            //sw.WriteLine("Numero de pontos utilizados: " + realWorldPointsToPlane.Count);
            //sw.WriteLine("Eq do plano: " + tablePlane.ToString());

            //sw.WriteLine("Min/Max:");
            //sw.WriteLine(" ---> MinX: " + bMinX + ", MaxX: " + bMaxX);
            //sw.WriteLine(" ---> MinY: " + bMinY + ", MaxY: " + bMaxY);
            //sw.WriteLine(" ---> MinZ: " + bMinZ + ", MaxZ: " + bMaxZ);

            //sw.WriteLine(" ------------------------------------ ");

            Debug.WriteLine("Histograms: ");
            for (int i = 0; i < 10; i++)
            {
                Debug.WriteLine("Histogram " + i);
                foreach (KeyValuePair<short, int> itens in histograms[i])
                {
                    Debug.WriteLine(itens.Key + ": " + itens.Value);
                }
                Debug.WriteLine(" --- ");
            }

            Debug.WriteLine("-------");

            sw.Close();
        }

        public void CalculateSurface()
        {
            Debug.WriteLine("> Histograms calculations");
            DateTime snapshot_start = DateTime.Now;
            DateTime now;

            Debug.WriteLine("    Taking Snapshots...");

            for (int i = 0; i < 500; i++)
            {
                short[] aux = new short[realDepthArray.Length];
                aux = (short[])realDepthArray.Clone();
                realDepthArrays.Add(aux);
                Thread.Sleep(30);
            }

            now = DateTime.Now;
            Debug.WriteLine("    Snapshots taken (" + now.Subtract(snapshot_start).TotalSeconds + " seconds).");

            DateTime histogram_start = DateTime.Now;
            Debug.WriteLine("    Calculating histogram per pixel...");

            // Need to calculate the distance found for each pixel
            // so we iterate on all of them
            for (int i = 0; i < 320 * 240; i++)
            {
                // Reset the histogram for this pixel
                Dictionary<short, int> histogram = new Dictionary<short, int>();

                // Iterate on all picture's snapshots
                foreach (short[] depths in realDepthArrays)
                {
                    // Here depths is the array with de depths
                    short depth_calculated = depths[i];

                    // If it is not a valid depth value, continue
                    if (depth_calculated == 0) continue;

                    // If it has in the histogram for this pixel, count 1 more
                    if (histogram.ContainsKey(depth_calculated))
                    {
                        histogram[depth_calculated]++;
                    }
                    // Or add the new depth found
                    else
                    {
                        histogram.Add(depth_calculated, 0);
                    }
                }

                short max_value = 0;
                int max_index = 0;
                // Now we calculate the d_max for this pixel
                foreach (KeyValuePair<short, int> par in histogram)
                {
                    if (par.Key > max_value)
                    {
                        max_value = par.Key;
                        max_index = par.Value;
                    }
                }

                histograms.Add(histogram);

                // Assigns d_surface to its position
                d_surfacePPixel[i] = max_value;
            }

            now = DateTime.Now;
            Debug.WriteLine("    Histograms and d_surface calculated (" + now.Subtract(histogram_start).TotalSeconds + " seconds)");

            // Warns the depth converter that we found what we wanted
            foundDepthsForSurface = true;
        }

        private void btnEncontrarDistancias_Click(object sender, RoutedEventArgs e)
        {
            Thread t = new Thread(new ThreadStart(CalculateSurface));
            t.IsBackground = true;
            t.Name = "Histograms Thread";
            t.Start();
        }
    }
}
