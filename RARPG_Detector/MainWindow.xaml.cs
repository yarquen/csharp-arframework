﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Research.Kinect.Nui;
using System.Collections;
using DotNetMatrix;
using RARPG_Detector.General;



namespace RARPG_Detector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region "    Attributes    "
        Runtime nui;
        int totalFrames = 0;
        int     lastFrames = 0;
        DateTime lastTime = DateTime.MaxValue;

        const int RED_IDX = 2;
        const int GREEN_IDX = 1;
        const int BLUE_IDX = 0;
        byte[] depthFrame32 = new byte[320 * 240 * 4];
        short[] realDepthArray = new short[320 * 240];
        byte[] plotter = new byte[320 * 240 * 4];
        int pointPosition;
        Microsoft.Research.Kinect.Nui.Vector[] pointsArea;

        ArrayList pontosParaPlano;

        bool calibrar;
        bool calibrado;
        

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            this.pointsArea = new Microsoft.Research.Kinect.Nui.Vector[4]; 
            this.pointPosition = 0;
            this.calibrar = false;
            this.calibrado = false;

            this.pontosParaPlano = new ArrayList();
        }

        #region init
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            nui = new Runtime();
            try
            {
                nui.Initialize(RuntimeOptions.UseDepthAndPlayerIndex | RuntimeOptions.UseSkeletalTracking | RuntimeOptions.UseColor | RuntimeOptions.UseDepth);
            }
            catch (InvalidOperationException ex)
            {
                global::System.Windows.Forms.MessageBox.Show(ex.Message);
                return;
            }

            try
            {
                nui.VideoStream.Open(ImageStreamType.Video, 2, ImageResolution.Resolution640x480, ImageType.Color);
                nui.DepthStream.Open(ImageStreamType.Depth, 2, ImageResolution.Resolution320x240, ImageType.DepthAndPlayerIndex);
            }
            catch (InvalidOperationException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return;
            }

            lastTime = DateTime.Now;

            nui.DepthFrameReady += new EventHandler<ImageFrameReadyEventArgs>(nui_DepthFrameReady);
            nui.VideoFrameReady += new EventHandler<ImageFrameReadyEventArgs>(nui_ColorFrameReady);
        }
        #endregion

        #region Mostrar Videos
        void nui_ColorFrameReady(object sender, ImageFrameReadyEventArgs e)
        {
            PlanarImage Image = e.ImageFrame.Image;
            canvasVideo.Source = BitmapSource.Create(Image.Width, Image.Height, 96, 96, PixelFormats.Bgr32, null, Image.Bits, Image.Width * Image.BytesPerPixel);
        }

        void nui_DepthFrameReady(object sender, ImageFrameReadyEventArgs e)
        {
            PlanarImage Image = e.ImageFrame.Image;
            byte[] convertedDepthFrame = convertDepthFrame(Image.Bits);
            canvasDepth.Source = BitmapSource.Create(Image.Width, Image.Height, 96, 96, PixelFormats.Bgr32, null, convertedDepthFrame, Image.Width * 4);

            ++totalFrames;

            DateTime cur = DateTime.Now;
            if (cur.Subtract(lastTime) > TimeSpan.FromSeconds(1))
            {
                int frameDiff = totalFrames - lastFrames;
                lastFrames = totalFrames;
                lastTime = cur;
                txtFPS.Text = frameDiff.ToString() + " fps";
            }


            if (planoSuperficie == null)
                return;

            byte[] pl = new byte[320 * 240 * 4];

            videoPlotter.Source = BitmapSource.Create(Image.Width, Image.Height, 96, 96, PixelFormats.Bgr32, null, plotter, Image.Width * 4);




        }
        #endregion


        void calibrarAmbiente()
        {
            for (int i = 0; i < 320; i++)
            {
                for (int j = 0; j < 240; j++)
                {
                    int depthX = (int)(320 * i / canvasVideo.ActualWidth);
                    int depthY = (int)(240 * j / canvasVideo.ActualHeight);
                    short depthValue = realDepthArray[depthY * 320 + depthX];
                    Microsoft.Research.Kinect.Nui.Vector v = nui.SkeletonEngine.DepthImageToSkeleton((float)depthX, (float)depthY, depthValue);

                    if (v.X > pointsArea[0].X && v.X < pointsArea[3].X)
                    {
                        if (v.Y < pointsArea[0].Y && v.Y > pointsArea[3].Y)
                        {
                            if (v.Z <= pointsArea[0].Z && v.Z >= pointsArea[3].Z)
                            {
                                //lblCalibrar.Content = "Calibrado";
                                this.calibrado = true;
                                this.calibrar = false;
                            }
                            else
                            {
                                //lblCalibrar.Content = "Erro ao Calibrar";
                                this.calibrar = false;
                                this.calibrado = false; 
                                return;
                            }
                        }
                    }
                }

            }
        }

        void reconhecerAmbiente()
        {

            for (int i = 0; i < 320; i++)
            {
                for (int j = 0; j < 240; j++)
                {
                    int depthX = (int)(320 * i / canvasVideo.ActualWidth);
                    int depthY = (int)(240 * j / canvasVideo.ActualHeight);
                    short depthValue = realDepthArray[depthY * 320 + depthX];
                    Microsoft.Research.Kinect.Nui.Vector v = nui.SkeletonEngine.DepthImageToSkeleton((float)depthX, (float)depthY, depthValue);

                    if (v.X > pointsArea[0].X && v.X < pointsArea[3].X)
                    {
                        if (v.Y < pointsArea[0].Y && v.Y > pointsArea[3].Y)
                        {
                            if (v.Z < pointsArea[3].Z)
                            {
                                //lblAux.Content = "Há uma mão";
                            }
                        }
                    }
                }
            }
        }

        byte[] convertDepthFrame(byte[] depthFrame16)
        {
            for (int i16 = 0, i32 = 0;
                i16 < depthFrame16.Length && i32 < depthFrame32.Length;
                i16 += 2, i32 += 4)
            {
                
                int player = depthFrame16[i16] & 0x07;
                int realDepth = (depthFrame16[i16 + 1] << 5) | (depthFrame16[i16] >> 3);
                realDepthArray[i16 / 2] = (short)realDepth;

                byte intensity = (byte)(255 - (255 * realDepth / 0x0fff));

                depthFrame32[i32 + RED_IDX] = intensity;
                depthFrame32[i32 + GREEN_IDX] = intensity;
                depthFrame32[i32 + BLUE_IDX] = intensity;   
            }
            return depthFrame32;

        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            nui.Uninitialize();
            Environment.Exit(0);
        }

        private void canvasDepth_MouseUp(object sender, MouseButtonEventArgs e)
        {
            getRealWorldPoint(e, canvasDepth, gridDepth);
        }

        private void canvasVideo_MouseUp(object sender, MouseButtonEventArgs e)
        {
            getRealWorldPoint(e, canvasVideo, gridVideo);
        }

        private void getRealWorldPoint(MouseButtonEventArgs e, Image i, Canvas c)
        {
            Point p = e.GetPosition(i);
            int depthX = (int)(320 * p.X / canvasVideo.ActualWidth);
            int depthY = (int)(240 * p.Y / canvasVideo.ActualHeight);
            short depthValue = realDepthArray[depthY * 320 + depthX];
            Microsoft.Research.Kinect.Nui.Vector v = nui.SkeletonEngine.DepthImageToSkeleton((float)depthX, (float)depthY, depthValue);

            //lblDistancia.Content = "{ x = " + v.X + ", y = " + v.Y + ", z = " + v.Z + "} Depth: " + depthValue;

            this.pontosParaPlano.Add(v);


            #region Adding visual point
            Rectangle r = new Rectangle();
            r.Width = 2; r.Height = 2;
            r.Fill = Brushes.Chocolate;
            Canvas.SetLeft(r, p.X);
            Canvas.SetTop(r, p.Y);
            
            c.Children.Add(r);
            #endregion
        }

        private void btnCalibrar_Click(object sender, RoutedEventArgs e)
        {
            this.calibrar = true; 
        }

        Plano planoSuperficie;

        private void btnEncontrarPlano_Click(object sender, RoutedEventArgs e)
        {
            GeneralMatrix M = new GeneralMatrix(pontosParaPlano.Count, 3);
            GeneralMatrix Mt /*Transposta*/, R = new GeneralMatrix(pontosParaPlano.Count, 1);

            for (int i = 0; i < pontosParaPlano.Count; i++)
            {
                M.SetElement(i, 0, ((Microsoft.Research.Kinect.Nui.Vector)pontosParaPlano[i]).X);
                M.SetElement(i, 1, ((Microsoft.Research.Kinect.Nui.Vector)pontosParaPlano[i]).Y);
                M.SetElement(i, 2, ((Microsoft.Research.Kinect.Nui.Vector)pontosParaPlano[i]).Z);
                R.SetElement(i, 0, -1); // Adding -1 to the right matrix
            }

            Mt = M.Transpose();

            // Multiplicando M pela sua transposta
            M = Mt.Multiply(M);
            // Multiplicando R pela transposta de M
            R = Mt.Multiply(R);

            GeneralMatrix I = M.Solve(R);

            planoSuperficie = new Plano()
            {
                A = I.GetElement(0, 0),
                B = I.GetElement(1, 0),
                C = I.GetElement(2, 0)
            };

            Console.WriteLine(planoSuperficie.ToString());


            //double[,] _M = new double[pontosParaPlano.Count, 3];
            //double[,] _R = new double[pontosParaPlano.Count, 1];
            //MathNet.Numerics.LinearAlgebra.Double.DenseMatrix M = new MathNet.Numerics.LinearAlgebra.Double.DenseMatrix(_M);
            //MathNet.Numerics.LinearAlgebra.Double.DenseMatrix Mt;
            //MathNet.Numerics.LinearAlgebra.Double.DenseMatrix R = new MathNet.Numerics.LinearAlgebra.Double.DenseMatrix(_R);


            //for (int i = 0; i < pontosParaPlano.Count; i++)
            //{
            //    M.SetRow(i, new double[] {
            //                ((Microsoft.Research.Kinect.Nui.Vector)pontosParaPlano[i]).X,
            //                ((Microsoft.Research.Kinect.Nui.Vector)pontosParaPlano[i]).Y,
            //                ((Microsoft.Research.Kinect.Nui.Vector)pontosParaPlano[i]).Z
            //            });
            //    R.SetRow(i, new double[] { -1 });
            //}

            //Mt = (MathNet.Numerics.LinearAlgebra.Double.DenseMatrix)M.Transpose();

            //// Multiplicando M pela sua transposta
            //M = (MathNet.Numerics.LinearAlgebra.Double.DenseMatrix)M.Multiply(Mt);
            //// Multiplicando R pela transposta de M
            //R = (MathNet.Numerics.LinearAlgebra.Double.DenseMatrix)R.Multiply(Mt);

            

        }
    }


    
}
