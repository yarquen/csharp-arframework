﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RARPG_Detector.General
{
    public class Plano
    {
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }
        public double D { get; set; }

        public override string ToString()
        {
            return "Plano: A=" + A + ", B=" + B + ", C=" + C + ", D=" + D;
        }

        public double pointIn(double x, double y, double z)
        {
            return (A * x) + (B * y) + (C * z) + D;
        }
    }
}
