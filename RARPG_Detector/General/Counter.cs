﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RARPG_Detector.General
{
    /// <summary>
    /// Represents a counter to check elapsed seconds or minutes.
    /// Example of usage:
    ///     NPTCounter counter = new NPTCounter;
    ///     if (counter.hasElapsed(0.5f, gameTimeSpan))
    ///         // Half second has elapsed
    /// </summary>
    public class RCounter
    {
        bool first;
        TimeSpan counter;

        public RCounter()
        {
            this.first = true;
        }

        /// <summary>
        /// Compares with the given TimeSpan object and checks if the seconds has elpased.
        /// </summary>
        /// <param name="seconds"></param>
        /// <param name="timer"></param>
        /// <returns></returns>
        public bool hasElapsed(float seconds, TimeSpan timer)
        {
            if (this.first)
            {
                this.counter = timer;
                this.first = false;
                return false;
            }

            string[] a = seconds.ToString().Split(".".ToArray());

            bool r = a.Length == 2 ?
                Math.Round(timer.Subtract(counter).TotalSeconds, Convert.ToInt32(a[1])) == seconds :
                Math.Round(timer.Subtract(counter).TotalSeconds, 1) == seconds;

            if (r) counter = timer;
            return r;
        }

        public float howMuchElapsed(TimeSpan timer)
        {
            return (float)timer.Subtract(this.counter).TotalSeconds;
        }

        public bool hasElapsed(int minutes, TimeSpan timer)
        {
            return hasElapsed((float)minutes * 60, timer);
        }

        public void setCounter(TimeSpan timer)
        {
            this.counter = timer;
        }
    }
}
