﻿using EixoX.ARFramework.Helpers;
using Emgu.CV.Structure;
using Microsoft.Research.Kinect.Nui;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EixoX.ARFramework.Interaction;
using System.Collections.ObjectModel;
using EixoX.ARFramework.Adapters;

namespace EixoX.ARFramework
{
    /// <summary>
    /// Represents a environment that wants to be touch-detected, using the Microsoft Kinect® device.
    /// This environment is composed by a Kinect® device, an array of interaction modes (that have the logic
    /// to determine a touch) and a surface calibrator (to map the interaction between the real world and the 
    /// digital screen).
    /// </summary>
    public class KinectTouchEnvironment
    {
        /// <summary>
        /// The Kinect device
        /// </summary>
        public Kinect Kinect { get; set; }
        /// <summary>
        /// Current interaction mode
        /// </summary>
        public KinectInteraction CurrentInteractionMode { get; set; }
        /// <summary>
        /// All of interaction modes available
        /// </summary>
        public List<KinectInteraction> InteractionModes { get; set; }
        /// <summary>
        /// THe surface calibrator, used for correct interaction
        /// </summary>
        public Calibrator SurfaceCalibrator { get; set; }

        
        
        /// <summary>
        /// Event Handler for Kinect's RGB image changing behavior
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void ColorImageChangedEventHandler(object sender, EventArgs e);
        public delegate void DepthImageChangedEventHandler(object sender, EventArgs e);

        public event ColorImageChangedEventHandler ColorImageChanged;
        public event DepthImageChangedEventHandler DepthImageChanged;

        /// <summary>
        /// Constructs an instance of a Kinect-based environment that detects touch
        /// </summary>
        /// <param name="interactions"></param>
        public KinectTouchEnvironment(params KinectInteraction[] interactions)
        {
            this.InteractionModes = new List<KinectInteraction>();

            foreach (KinectInteraction interaction in interactions)
                this.InteractionModes.Add(interaction);
        }

        /// <summary>
        /// Starts the environment
        /// </summary>
        public void Start()
        {
            if (this.Kinect == null)
                this.Kinect = new Kinect();

            this.Kinect.Initialize();
            this.Kinect.SetColorCallback(ColorFrameReady);
            this.Kinect.SetDepthCallback(DepthFrameReady);
        }

        /// <summary>
        /// Method used when the RGB-Color based frame from Kinect is ready to be processed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Image event argument</param>
        public void ColorFrameReady(object sender, ImageFrameReadyEventArgs e)
        {
            if (ColorImageChanged == null)
                throw new Exception("Must assing an event handler for RGB video processing");

            ColorImageChanged(this.CurrentInteractionMode.ProcessColorFrame(sender, e), e);
        }

        /// <summary>
        /// Method used when the Depth frame is ready to be processed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Image event argument</param>
        public void DepthFrameReady(object sender, ImageFrameReadyEventArgs e)
        {
            if (DepthImageChanged == null)
                throw new Exception("Must assing an event handler for depth video processing");

            DepthImageChanged(this.CurrentInteractionMode.ProcessDepthFrame(sender, e), e);
        }
    }
}
