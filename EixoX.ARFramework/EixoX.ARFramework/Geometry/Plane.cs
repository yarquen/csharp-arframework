﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EixoX.ARFramework.Geometry
{
    /// <summary>
    /// Abstraction of a geometry's plane with 3 dimensions
    /// </summary>
    public class Plane
    {
        // Coefficients
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }
        public double D { get; set; }

        public double PointIn(double x, double y, double z)
        {
            return (A * x) + (B * y) + (C * z) + D;
        }

        public override string ToString()
        {
            return String.Format("{A: {0}, B: {1}, C: {2}, D: {3}}", A, B, C, D);
        }
    }
}
