﻿using EixoX.ARFramework.Adapters;
using Microsoft.Research.Kinect.Nui;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EixoX.ARFramework.Interaction
{
    public abstract class KinectInteraction
    {
        protected const int RED_IDX = 2;                              // Index to access Red color on a RGB byte
        protected const int GREEN_IDX = 1;                            // Index to access Green color on a RGB byte
        protected const int BLUE_IDX = 0;                             // Index to access Blue channel on a RGB byte


        /// <summary>
        /// List containing all touches detected
        /// </summary>
        public ObservableCollection<Rectangle> Interactions { get; set; }
        /// <summary>
        /// Adapter for returning RGB video and Depth video
        /// </summary>
        public KinectStreamAdapter Adapter { get; set; }
        /// <summary>
        /// Process the color frame provided by the Kinect Sensor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public abstract object ProcessColorFrame(object sender, ImageFrameReadyEventArgs e);
        /// <summary>
        /// Processes the depthframe provided by the Kinect Sensor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public abstract object ProcessDepthFrame(object sender, ImageFrameReadyEventArgs e);
        /// <summary>
        /// The view to debug things
        /// </summary>
        public IInteractionView View { get; set; }
        
        public abstract string Name
        {
            get;
        }

        public KinectInteraction()
        {
            this.Adapter = new DefaultAdapter();
        }
        public KinectInteraction(KinectStreamAdapter adapter)
        {
            this.Adapter = adapter;
        }
        public KinectInteraction(KinectStreamAdapter adapter, IInteractionView view)
        {
            this.Adapter = adapter;
            this.View = view;
        }
    }
}
