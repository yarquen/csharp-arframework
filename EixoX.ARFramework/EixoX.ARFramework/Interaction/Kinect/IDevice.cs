﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EixoX.ARFramework.Interaction
{
    /// <summary>
    /// Represents an interaction device
    /// </summary>
    public interface IDevice
    {
        /// <summary>
        /// Tilts the device in a given direction
        /// </summary>
        /// <param name="direction">Direction to tilt the device</param>
        void Tilt(TiltDirection direction);
        /// <summary>
        /// Initializes the device
        /// </summary>
        void Initialize();
        /// <summary>
        /// Unitializes the device
        /// </summary>
        void Unitialize();
        /// <summary>
        /// Checks if the device is active
        /// </summary>
        /// <returns>true if it is, false otherwise</returns>
        bool IsActive();
    }
}
