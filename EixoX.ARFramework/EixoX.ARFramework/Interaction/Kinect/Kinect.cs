﻿using EixoX.ARFramework.Helpers;
using Emgu.CV.Structure;
using Microsoft.Research.Kinect.Nui;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EixoX.ARFramework.Interaction
{
    /// <summary>
    /// Microsoft Kinect® device abstraction, based on the Microsoft.Research dll.
    /// </summary>
    public class Kinect : IDevice
    {
        private Runtime KinectRuntime { get; set; }
        private int Angle;

        public Kinect()
        {
            for (int i = 0; i < Runtime.Kinects.Count; i++)
                if (Runtime.Kinects[i].Status == KinectStatus.Connected)
                {
                    this.KinectRuntime = Runtime.Kinects[i];
                    break;
                }

            Angle = 0;
        }

        public void SetDepthCallback(EventHandler<ImageFrameReadyEventArgs> handler)
        {
            this.KinectRuntime.DepthFrameReady += handler;
        }

        public void SetColorCallback(EventHandler<ImageFrameReadyEventArgs> handler)
        {
            this.KinectRuntime.VideoFrameReady += handler;
        }

        public void Tilt(TiltDirection direction)
        {
            switch (direction)
            {
                case TiltDirection.UP:
                    if (this.KinectRuntime.NuiCamera.ElevationAngle == Camera.ElevationMaximum)
                        return;

                    this.KinectRuntime.NuiCamera.ElevationAngle = ++Angle;
                    break;
                case TiltDirection.DOWN:
                    if (this.KinectRuntime.NuiCamera.ElevationAngle == Camera.ElevationMinimum)
                        return;

                    this.KinectRuntime.NuiCamera.ElevationAngle = --Angle;
                    break;
            }
        }

        public void Initialize()
        {
            if (this.KinectRuntime == null)
                throw new ArgumentException("Kinect not connected");

            this.KinectRuntime.Initialize(RuntimeOptions.UseDepthAndPlayerIndex | RuntimeOptions.UseSkeletalTracking | RuntimeOptions.UseColor | RuntimeOptions.UseDepth);
            this.KinectRuntime.VideoStream.Open(ImageStreamType.Video, 2, ImageResolution.Resolution640x480, ImageType.Color);
            this.KinectRuntime.DepthStream.Open(ImageStreamType.Depth, 2, ImageResolution.Resolution320x240, ImageType.Depth);
        }

        public void Unitialize()
        {
            this.KinectRuntime.Uninitialize();
        }

        public bool IsActive()
        {
            return this.KinectRuntime.Status == KinectStatus.Connected;
        }
    }
}
