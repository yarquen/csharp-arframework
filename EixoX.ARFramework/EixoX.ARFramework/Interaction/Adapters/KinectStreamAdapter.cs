﻿using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EixoX.ARFramework.Adapters
{
    /// <summary>
    /// 
    /// </summary>
    public interface KinectStreamAdapter
    {
        object RgbAdapt(Emgu.CV.Image<Rgb, Byte> param);
        object DepthAdapt(Emgu.CV.Image<Rgb, Byte> param);
    }
}
