﻿using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace EixoX.ARFramework.Adapters
{
    /// <summary>
    /// Adapter class for using Windows Presentation Foundation
    /// </summary>
    public class WPFAdapter : KinectStreamAdapter
    {
        public object RgbAdapt(Emgu.CV.Image<Rgb, byte> param)
        {
            return ToBitmapSource(param);
        }

        public object DepthAdapt(Emgu.CV.Image<Rgb, byte> param)
        {
            return ToBitmapSource(param);
        }

        /// <summary>
        /// Converts an Emgu.CV.Image<Rgb, Byte> image into a System.Windows.Media.Imaging.BitmapSource image
        /// </summary>
        /// <param name="cvImage"></param>
        /// <returns></returns>
        public static BitmapSource ToBitmapSource(Emgu.CV.Image<Rgb, Byte> cvImage)
        {
            using (System.Drawing.Bitmap source = cvImage.Bitmap)
            {
                IntPtr ptr = source.GetHbitmap(); //obtain the Hbitmap

                BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    ptr,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

                DeleteObject(ptr);
                return bs;
            }
        }

        /// <summary>
        /// Delete a GDI object
        /// </summary>
        /// <param name="o">The poniter to the GDI object to be deleted</param>
        /// <returns></returns>
        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr o);
    }
}
