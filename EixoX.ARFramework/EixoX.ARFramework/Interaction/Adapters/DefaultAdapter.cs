﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EixoX.ARFramework.Adapters
{
    public class DefaultAdapter : KinectStreamAdapter
    {
        public object RgbAdapt(Emgu.CV.Image<Emgu.CV.Structure.Rgb, byte> param)
        {
            return param.Bitmap;
        }

        public object DepthAdapt(Emgu.CV.Image<Emgu.CV.Structure.Rgb, byte> param)
        {
            return param.Bitmap;
        }
    }
}
