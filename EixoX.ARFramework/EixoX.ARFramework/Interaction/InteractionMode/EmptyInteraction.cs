﻿using EixoX.ARFramework.Adapters;
using EixoX.ARFramework.Helpers;
using Emgu.CV.Structure;
using Microsoft.Research.Kinect.Nui;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace EixoX.ARFramework.Interaction
{
    public class EmptyInteraction : KinectInteraction
    {
        short[] realDepthArray = new short[320 * 240];      // Array to store depth information provided by the sensor

        public EmptyInteraction()
        {
            this.Interactions = new System.Collections.ObjectModel.ObservableCollection<Rectangle>();
        }
        public EmptyInteraction(KinectStreamAdapter adapter)
            : base(adapter)
        {
            this.Interactions = new System.Collections.ObjectModel.ObservableCollection<Rectangle>();
        }

        public override object ProcessColorFrame(object sender, Microsoft.Research.Kinect.Nui.ImageFrameReadyEventArgs e)
        {
            PlanarImage Image = e.ImageFrame.Image;
            Bitmap bmp = ImageHelper.ToBitmap(Image);

            Emgu.CV.Image<Rgb, byte> img = new Emgu.CV.Image<Rgb, byte>(bmp);
            img = img.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);

            return this.Adapter.RgbAdapt(img);
        }

        public override object ProcessDepthFrame(object sender, Microsoft.Research.Kinect.Nui.ImageFrameReadyEventArgs e)
        {
            PlanarImage Image = e.ImageFrame.Image;
            byte[] convertedDepthFrame = ConvertDepthFrame(Image.Bits);

            BitmapSource bs = BitmapSource.Create(Image.Width, Image.Height, 96, 96, PixelFormats.Bgr32, null, convertedDepthFrame, Image.Width * 4);   
            Emgu.CV.Image<Rgb, Byte> img = new Emgu.CV.Image<Rgb, Byte>(ImageHelper.ToBitmap(bs));

            return this.Adapter.DepthAdapt(img);
        }

        /// <summary>
        /// Converts a depth frame from 16-bit to 32-bit (in gray scale mode)
        /// </summary>
        /// <param name="depthFrame16">the depths in a 16-bit array</param>
        /// <returns>the array converted to 32-bit</returns>
        private byte[] ConvertDepthFrame(byte[] depthFrame16)
        {
            byte[] depthFrame32 = new byte[320 * 240 * 4];

            for (int i16 = 0, i32 = 0; i16 < depthFrame16.Length && i32 < depthFrame32.Length; i16 += 2, i32 += 4)
            {
                int realDepth = (int)depthFrame16[i16] | depthFrame16[i16 + 1] << 8;
                //realDepthArray[i16 / 2] = (short)realDepth;
                
                byte intensity = (byte)(255 - (255 * realDepth / 0x0fff));

                depthFrame32[i32 + RED_IDX] = intensity;
                depthFrame32[i32 + GREEN_IDX] = intensity;
                depthFrame32[i32 + BLUE_IDX] = intensity;
            }

            return depthFrame32;
        }

        public override string Name
        {
            get { return "Empty"; }
        }
    }
}
