﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EixoX.ARFramework.Interaction
{
    /// <summary>
    /// 
    /// </summary>
    public interface IInteractionView
    {
        /// <summary>
        /// Write something to user.
        /// </summary>
        /// <param name="value">value to write</param>
        void WriteLine(string value);
    }
}
