﻿using EixoX.ARFramework.Adapters;
using EixoX.ARFramework.Helpers;
using Emgu.CV.Structure;
using Microsoft.Research.Kinect.Nui;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace EixoX.ARFramework.Interaction
{
    /// <summary>
    /// 
    /// </summary>
    public class SimpleDepth : KinectInteraction
    {
        /// <summary>
        /// Array to store real depth information data gathered from Kinect
        /// </summary>
        public short[] RealDepth { get; private set; }
        /// <summary>
        /// Dictionary to store histogram of distances, for each depth image's pixels
        /// </summary>
        public List<Dictionary<short, int>> Histograms {  get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public short[] CalculatedDistancePerPixel { get; private set; }
        /// <summary>
        /// Flag to avoid errors when not calculated all of histogram values
        /// </summary>
        public bool FoundDepths { get; private set; }
        /// <summary>
        /// Aux list to store real depth information
        /// </summary>
        List<short[]> RealDepthArrays = new List<short[]>();

        /// <summary>
        /// Constructs a new instance of Simple Depth interaction mode.
        /// </summary>
        /// <param name="depthImageSize">Depth image's size.</param>
        public SimpleDepth(Size depthImageSize) 
            : this(depthImageSize, null, null) { }
        public SimpleDepth(Size depthImageSize, KinectStreamAdapter adapter, IInteractionView view)
            : base(adapter, view)
        {
            int depthSize = depthImageSize.Width * depthImageSize.Height;
            this.RealDepth = new short[depthSize];
            this.Histograms = new List<Dictionary<short, int>>();
            this.CalculatedDistancePerPixel = new short[depthSize];
        }

        private void CalculateHistograms()
        {
            this.View.WriteLine("> Histograms calculations");
            DateTime snapshot_start = DateTime.Now, now;

            this.View.WriteLine("    Taking Snapshots...");

            for (int i = 0; i < 500; i++)
            {
                short[] aux = new short[RealDepth.Length];
                aux = (short[])RealDepth.Clone();
                RealDepthArrays.Add(aux);
                Thread.Sleep(30);
            }

            now = DateTime.Now;
            this.View.WriteLine("    Snapshots taken (" + now.Subtract(snapshot_start).TotalSeconds + " seconds).");

            DateTime histogram_start = DateTime.Now;
            this.View.WriteLine("    Calculating histogram per pixel...");

            // Need to calculate the distance found for each pixel
            // so we iterate on all of them
            for (int i = 0; i < RealDepth.Length; i++)
            {
                // Reset the histogram for this pixel
                Dictionary<short, int> histogram = new Dictionary<short, int>();

                // Iterate on all picture's snapshots
                foreach (short[] depths in RealDepthArrays)
                {
                    // Here depths is the array with de depths
                    short depth_calculated = depths[i];

                    // If it is not a valid depth value, continue
                    if (depth_calculated == 0) continue;

                    // If it has in the histogram for this pixel, count 1 more
                    if (histogram.ContainsKey(depth_calculated))
                        histogram[depth_calculated]++;
                    // Or add the new depth found
                    else
                        histogram.Add(depth_calculated, 0);
                }

                short max_value = 0;
                int max_index = 0;
                // Now we calculate the d_max for this pixel
                foreach (KeyValuePair<short, int> par in histogram)
                    if (par.Key > max_value)
                    {
                        max_value = par.Key;
                        max_index = par.Value;
                    }

                Histograms.Add(histogram);

                // Assigns d_surface to its position
                CalculatedDistancePerPixel[i] = max_value;
            }

            now = DateTime.Now;
            this.View.WriteLine("    Histograms and d_surface calculated (" + now.Subtract(histogram_start).TotalSeconds + " seconds)");

            // Warns the depth converter that we found what we wanted
            this.FoundDepths = true;
        }

        /// <summary>
        /// Initializes interaction. Do the calculations and changes the flag.
        /// </summary>
        public void InitMode()
        {
            Thread t = new Thread(new ThreadStart(CalculateHistograms));
            t.IsBackground = true;
            t.Name = "Histograms Thread";
            t.Start();
        }

        public override object ProcessColorFrame(object sender, ImageFrameReadyEventArgs e)
        {
            PlanarImage Image = e.ImageFrame.Image;
            Bitmap bmp = ImageHelper.ToBitmap(Image);

            Emgu.CV.Image<Rgb, byte> img = new Emgu.CV.Image<Rgb, byte>(bmp);
            img = img.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);

            return this.Adapter.RgbAdapt(img);
        }

        public override object ProcessDepthFrame(object sender, ImageFrameReadyEventArgs e)
        {
            PlanarImage Image = e.ImageFrame.Image;
            byte[] convertedDepthFrame = ConvertDepthFrame(Image.Bits);

            BitmapSource bs = BitmapSource.Create(Image.Width, Image.Height, 96, 96, PixelFormats.Bgr32, null, convertedDepthFrame, Image.Width * 4);
            Emgu.CV.Image<Rgb, Byte> img = new Emgu.CV.Image<Rgb, Byte>(ImageHelper.ToBitmap(bs));

            img.SmoothMedian(5);
            img.Erode(2);
            //img.Dilate(2);

            Emgu.CV.Image<Gray, Byte> transformedImage = img.Convert<Gray, Byte>();
            Emgu.CV.Contour<System.Drawing.Point> contoursFound = transformedImage.FindContours();

            if (contoursFound != null)
            {
                //this.View.WriteLine("Founded contours: ");
                //this.View.WriteLine(contoursFound.BoundingRectangle.ToString());
            }

            return this.Adapter.DepthAdapt(img);
        }

        /// <summary>
        /// Converts a depth frame from 16-bit to 32-bit (in gray scale mode)
        /// </summary>
        /// <param name="depthFrame16">the depths in a 16-bit array</param>
        /// <returns>the array converted to 32-bit</returns>
        private byte[] ConvertDepthFrame(byte[] depthFrame16)
        {
            byte[] depthFrame32 = new byte[320 * 240 * 4];

            int iDepth = 0;
            for (int i16 = 0, i32 = 0; i16 < depthFrame16.Length && i32 < depthFrame32.Length; i16 += 2, i32 += 4)
            {
                int realDepth = (int)depthFrame16[i16] | depthFrame16[i16 + 1] << 8;

                RealDepth[i16 / 2] = (short)realDepth;

                byte intensity = (byte)(255 - (255 * realDepth / 0x0fff));

                if (!this.FoundDepths)
                {
                    depthFrame32[i32 + RED_IDX] = 0;
                    depthFrame32[i32 + GREEN_IDX] = 0;
                    depthFrame32[i32 + BLUE_IDX] = 0;
                    continue;
                }

                short surface = this.CalculatedDistancePerPixel[iDepth++];

                if (realDepth < surface - 10 && realDepth > surface - 20)
                {
                    depthFrame32[i32 + RED_IDX] = 255;
                    depthFrame32[i32 + GREEN_IDX] = 255;
                    depthFrame32[i32 + BLUE_IDX] = 255;
                }
                else
                {
                    depthFrame32[i32 + RED_IDX] = 0;
                    depthFrame32[i32 + GREEN_IDX] = 0;
                    depthFrame32[i32 + BLUE_IDX] = 0;
                }
            }

            return depthFrame32;
        }

        public override string Name
        {
            get { return "SimpleDepth"; }
        }
    }
}
