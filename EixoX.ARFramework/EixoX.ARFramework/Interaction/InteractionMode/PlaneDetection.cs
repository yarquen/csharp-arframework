﻿using EixoX.ARFramework.Helpers;
using Emgu.CV.Structure;
using Microsoft.Research.Kinect.Nui;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EixoX.ARFramework.Interaction
{
    public class PlaneDetection : KinectInteraction
    {
        public override object ProcessColorFrame(object sender, ImageFrameReadyEventArgs e)
        {
            PlanarImage Image = e.ImageFrame.Image;
            Bitmap bmp = ImageHelper.ToBitmap(Image);

            Emgu.CV.Image<Rgb, byte> img = new Emgu.CV.Image<Rgb, byte>(bmp);
            img = img.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);

            return img.Bitmap;
        }

        public override object ProcessDepthFrame(object sender, ImageFrameReadyEventArgs e)
        {
            //PlanarImage Image = e.ImageFrame.Image;
            
            //// With Plane Detection
            //byte[] convertedDepthFrame = convertDepthFrame(Image.Bits);

            //BitmapSource bs = BitmapSource.Create(Image.Width, Image.Height, 96, 96, PixelFormats.Bgr32, null, convertedDepthFrame, Image.Width * 4);

            //Emgu.CV.Image<Rgb, Byte> img = new Emgu.CV.Image<Rgb, Byte>(_bitmapFromSource(bs));

            //img = img.SmoothMedian(3);
            ////img = img.Erode(1);
            //img = img.Dilate(1);

            //Emgu.CV.Image<Gray, Byte> img_novo = img.Convert<Gray, Byte>();

            //Emgu.CV.Contour<System.Drawing.Point> contornos = img_novo.FindContours();

            //if (contornos != null && tablePlane != null)
            //{
            //    //Debug.WriteLine(">> Contornos encontrados.");
            //    //Debug.WriteLine(contornos.BoundingRectangle.ToString());
            //    //RARPG_Projection.SetRectOn(getScreenPoint(new Point(contornos.BoundingRectangle.X, contornos.BoundingRectangle.Y)));
            //    //Point p = getScreenPoint(new Point(contornos.BoundingRectangle.X, contornos.BoundingRectangle.Y));
            //    //Debug.WriteLine("Clique em " + p.ToString());
            //    //LeftClick((int)p.X, (int)p.Y);
            //}

            //videoDepth.Source = ToBitmapSource(img);
            throw new NotImplementedException();
        }

        public override string Name
        {
            get { return "Plane"; }
        }
    }
}
