﻿using Microsoft.Research.Kinect.Nui;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace EixoX.ARFramework.Helpers
{
    /// <summary>
    /// Encapsulates some useful functions for images
    /// </summary>
    public static class ImageHelper
    {
        /// <summary>
        /// Converts a Microsoft.Research.Kinect.Nui.PlanarImage to a System.Drawing.Bitmap image
        /// </summary>
        /// <param name="planarImage">the PlanarImage source</param>
        /// <returns>the bitmap converted image</returns>
        public static Bitmap ToBitmap(PlanarImage planarImage)
        {
            Bitmap bmap = new Bitmap(planarImage.Width, planarImage.Height, PixelFormat.Format32bppRgb);

            BitmapData bmapdata = bmap.LockBits(new Rectangle(0, 0, planarImage.Width, planarImage.Height),
                                                ImageLockMode.WriteOnly,
                                                bmap.PixelFormat);

            IntPtr ptr = bmapdata.Scan0;
            
            Marshal.Copy(
                planarImage.Bits,                                                   // byte[] source
                0,                                                                  // start index
                ptr,                                                                // destination
                planarImage.Width * planarImage.BytesPerPixel * planarImage.Height  // length
                );
            
            bmap.UnlockBits(bmapdata);

            return bmap;
        }
        /// <summary>
        /// Creates a System.Drawing.Bitmap image from an array of bytes
        /// </summary>
        /// <param name="array">the source array</param>
        /// <param name="width">bitmap's width</param>
        /// <param name="height">bitmap's height</param>
        /// <param name="format">pixel format of the image</param>
        /// <param name="bytesPerPixel">how many bytes per pixel should the image have</param>
        /// <returns></returns>
        public static Bitmap ToBitmap(byte[] array, int width, int height, PixelFormat format, int bytesPerPixel)
        {
            Bitmap bmap = new Bitmap(width, height, format);
            BitmapData dataAux = bmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, bmap.PixelFormat);
            IntPtr ptr = dataAux.Scan0;
            Marshal.Copy(array, 0, ptr, width * height * 4);
            bmap.UnlockBits(dataAux);

            return bmap;
        }
        /// <summary>
        /// Converts a System.Windows.Media.Imaging.BitmapSource image into a System.Drawing.Bitmap image
        /// </summary>
        /// <param name="bitmapsource">BitmapSource source image</param>
        /// <returns>Converted Bitmap image</returns>
        public static Bitmap ToBitmap(BitmapSource bitmapsource)
        {
            System.Drawing.Bitmap bitmap;
            using (MemoryStream outStream = new MemoryStream())
            {
                // from System.Media.BitmapImage to System.Drawing.Bitmap 
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new System.Drawing.Bitmap(outStream);
            }
            return bitmap;
        }
    }
}
