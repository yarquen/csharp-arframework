﻿using EixoX.ARFramework.Adapters;
using EixoX.ARFramework.Interaction;
using Microsoft.Research.Kinect.Nui;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EixoX.ARFramework.WPFSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IInteractionView
    {
        /// <summary>
        /// The touch-based environment that we'll use
        /// </summary>
        KinectTouchEnvironment Environment;

        public MainWindow()
        {
            InitializeComponent();

            // Initializing the Environment
            this.Environment = new KinectTouchEnvironment(new EmptyInteraction(new WPFAdapter()),
                                                          new SimpleDepth(new System.Drawing.Size(320, 240), new WPFAdapter(), this));

            this.Environment.CurrentInteractionMode = this.Environment.InteractionModes[0];
        }

        private void InitButton_Click_1(object sender, RoutedEventArgs e)
        {
            this.Environment.Start();

            this.Environment.CurrentInteractionMode.Interactions.CollectionChanged += Interactions_CollectionChanged;
            this.Environment.ColorImageChanged += Environment_ColorImageChanged;
            this.Environment.DepthImageChanged += Environment_DepthImageChanged;
        }

        private void Interactions_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            foreach (System.Drawing.Rectangle rect in this.Environment.CurrentInteractionMode.Interactions)
                DebugArea.Text += string.Concat("Touch in: ", rect.ToString(), "\n");
        }

        void Environment_DepthImageChanged(object sender, EventArgs e)
        {
            videoDepth.Source = (BitmapSource)sender;
        }

        void Environment_ColorImageChanged(object sender, EventArgs e)
        {
            videoRGB.Source = (BitmapSource)sender;
        }

        private void TiltButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Environment.Kinect.Tilt(button.Name.Contains("Up") ? TiltDirection.UP : TiltDirection.DOWN);
        }

        public void WriteLine(string value)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                DebugArea.Text += value + "\n";
            }));
        }

        private void DepthInteraction_Click_1(object sender, RoutedEventArgs e)
        {
            this.Environment.CurrentInteractionMode = this.Environment.InteractionModes
                                                                      .Where(x => x.Name.Equals("SimpleDepth", StringComparison.OrdinalIgnoreCase))
                                                                      .SingleOrDefault();
        }

        private void AboutButton_Click_1(object sender, RoutedEventArgs e)
        {
            About aboutWindow = new About();
            aboutWindow.ShowDialog();
        }

        private void DepthInteractionStartButton_Click_1(object sender, RoutedEventArgs e)
        {
            ((SimpleDepth)this.Environment.CurrentInteractionMode).InitMode();
        }

        private void DebugArea_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            //DebugArea.ScrollToEnd();
        }
    }
}
